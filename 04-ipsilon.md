Ipsilon evaluation
==================

[Ipsilon](https://ipsilon-project.org) is an identity provider server and toolkit.
It is written in python and uses the cherrypy web development toolkit. Development
began in 2014 and version 2.1 was released in november 2017.

# Installation

First some dependencies have to be installed.
```
apt install python-cherrypy3 python-lxml python-lasso python-requests python-jwcrypto python-ldap
git clone https://pagure.io/ipsilon.git
cd ipsilon
./quickrun.py
```

With the quickrun.py script a test instance of ipsilon listens on 0.0.0.0:8080. With
the username 'admin' and the password 'ipsilon' one can log in to the test instance.

![Screenshot of ipsilon](04-ipsilon-01.png)

The interface seems very clean and easy to navigate.

Because ipsilon only provides the possibility to have one LDAP backend, no further
tests have been done. Also, there is no documentation whatsoever on installing
ipsilon (the release archive file provides a setup.py file though).
