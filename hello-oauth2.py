# copied from https://requests-oauthlib.readthedocs.io/en/latest/examples/real_world_example.html
from requests_oauthlib import OAuth2Session
from flask import Flask, request, redirect, session, url_for
from flask.json import jsonify
import os

app = Flask(__name__)
app.secret_key = os.urandom(24)

redirect_uri = 'http://your.test.script.tld/callback'

# Keycloak
client_id = 'client-id'
client_secret = 'client-secret'
site = 'http://auth.example.com'
authorization_base_url = site + '/auth/realms/master/protocol/openid-connect/auth'
userinfo_url = site + '/auth/realms/master/protocol/openid-connect/userinfo'
token_url = site + '/auth/realms/master/protocol/openid-connect/token'
scope = []

# LLNG
#client_id = "client-id"
#client_secret = "client-secret"
#site = 'http://auth.example.com'
#authorization_base_url = site + '/oauth2/authorize'
#userinfo_url = site + '/oauth2/userinfo'
#token_url = site + '/oauth2/token'
#scope = ['openid', 'profile', 'email', 'address']

@app.route("/")
def demo():
    """Step 1: User Authorization.

    Redirect the user/resource owner to the OAuth provider (i.e. Github)
    using an URL with a few key OAuth parameters.
    """
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    oauth = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=scope)
    authorization_url, state = oauth.authorization_url(authorization_base_url)

    # State is used to prevent CSRF, keep this for later.
    session['oauth_state'] = state
    return redirect(authorization_url)


# Step 2: User authorization, this happens on the provider.

@app.route("/callback", methods=["GET"])
def callback():
    """ Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    oauth = OAuth2Session(client_id, state=session['oauth_state'], redirect_uri=redirect_uri)
    token = oauth.fetch_token(token_url, client_secret=client_secret,
                               authorization_response=request.url)
    # At this point you can fetch protected resources but lets save
    # the token and show how this is done from a persisted token
    # in /profile.
    session['oauth_token'] = token

    return redirect(url_for('.profile'))


@app.route("/profile", methods=["GET"])
def profile():
    """Fetching a protected resource using an OAuth 2 token.
    """
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    oauth = OAuth2Session(client_id, token=session['oauth_token'])
    return jsonify(oauth.get(userinfo_url).json())


if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    app.run(debug=True)

