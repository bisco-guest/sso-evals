Glewlwyd evaluation
===================

[Glewlwyd](https://babelouest.github.io/glewlwyd/) is an Oauth2 authentication
server. It is distributed under the GPL v3. The first stable release was 1.0 in
march 2017, according to the github release page. The current release is 1.4.4
which was released on May 20. Glewlwyd is developed by babelouest, but there
also have been a handful commits of other contributors.  Glewlwyd is written in
C and depends on a couple of project that are also maintained by babelouest.
There is also a debian package for glewlwyd in Debian testing that is
maintained by babelouest.

# Installation
On a debian stretch system one has to add the sources for buster. It is
advisable to use apt pinning to give the buster packages a lower priority:
```
Package: *
Pin: release a=testing
Pin-Priority: 600

Package: *
Pin: release a=stable
Pin-Priority: 800
```
The installation is done with:
```
apt install -t testing glewlwyd
```

During the package installation the database for glewlwyd is being set up. The
default is to use a sqlite database. The default path for the sqlite db is
`/var/lib/dbconfig-common/sqlite3/glewlwyd/glewlwyd`.

# Basic Configuration

The default configuration file is `/etc/glewlwyd/glewlwyd-debian.conf`. By
default glewlwyd listens on http://localhost:4593. A nginx configuration for
proxying the traffic to glewlwyd could look like this:
```
        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri @auth; #$uri/ =404;
        }

        location @auth {
          proxy_pass http://glewlwyd_server;

          proxy_http_version 1.1;

          proxy_set_header Host               $host;
          proxy_set_header X-Real-IP          $remote_addr;
          proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto  $scheme;
          proxy_set_header   Authorization $http_authorization;
          proxy_pass_header  Authorization;
        }
```

glewlwyd comes with a webinterface that also provides means for configuring
clients, users, scopes, etc. The default admin account is `admin:password`.

![Screenshot of the glewlwyd client setup](03-Glewlwyd-01.png)

The interface is very clean and modern and uses a lot of javascript. Sometimes
the webpage loads without content and then the javascript loads after a moment
and shows the content. One major disadvantage is, that it does not provide the
means for multiple LDAP backends yet. There is a version 2 in the works, but
its not sure that it will be ready before the end of GSOC.
Attempts to set glewlwyd up with either gitlab or the python/flask test application
were not successful yet.

# Overall

The user interface is modern and well-arranged. The Bus factor (only one person
is upstream for the software, some dependencies and the Debian packages) is a
drawback. There is also the fact, that in the current version multiple LDAP
backends are not possible yet, which is something like a hard requirement for
the use in the Debian project.
