Evaluations of SSO solutions
============================


* [Keycloak evaluation](01-Keycloak.md)
* [Lemonldap-NG evaluation](02-lemonldap.md)
* [Glewlwyd evaluation](03-glewlwyd.md)
* [Ipsilon evaluation](04-ipsilon.md)

| Product      | Packaged[^pack]   | Language | Supports Multiple Backends | Ease of Installation | Ease of Configuration | Support    | Interface |
| :----------: | :------:          | :------: | :------------------------: | :------------------: | :-------------------: | :--------: | :-------: |
| Keycloak     | :x:               | java     | :heavy_check_mark:         |       8/10           |         9/10          | not needed |   10/10   |
| Lemonldap-NG | :heavy_check_mark:| perl     | :heavy_check_mark:         |       10/10          |         6/10          |   10/10    |   6/10    |
| Glewlwyd     | :heavy_check_mark:| c        | :x:                        |       10/10          |         7/10          |   10/10    |   7/10    |
| Ipsilon      | :x:               | python   | :x:                        |       1/10           |         1/10          | not tried  |   7/10    |

[^pack]: this refers to Debian packages
